-- deets: aug.lua
-- Copyright (C) 2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

require("augeas")
require("resource")

local ipairs = ipairs
local augeas = augeas
local print = print -- remove this in new phase
local register_resource = register_resource

augobj = augeas.init()
local augobj = augobj

module("aug")

function iscompliant(apath, gota)
  local desiredstatus = gota.ensure
  local desiredvalue = gota.value
  local status = augeas.get(augobj, apath)

  if (desiredstatus == "set") then
     if (not status) then
        return false
     elseif (status == desiredvalue) then
	return true
     else
        return false
     end
  elseif (desiredstatus == "unset") then
     if (not status) then
        return true
     else
        return false
     end
  end

  return false
end

function rectify(apath, gota)
  local desiredstatus = gota.ensure
  local desiredvalue = gota.value
  if (not desiredvalue) then desiredvalue = "" end
  if (desiredstatus == "set") then
	return "augtool -s set "..apath.." "..desiredvalue
  elseif (desiredstatus == "unset") then
	return "augtool -s rm "..apath
  end
end

function root(aplist, gotita)
  for _,ap in ipairs(aplist) do
    if (not register_resource("aug", ap, gotita)) then
        print("Registration error", ap)
    end
  end
end
