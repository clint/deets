-- deets: resource.lua
-- Copyright (C) 2009-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

require("utils-table")

defaultprovider = {
   pkg="apt"
 , file="file"
 , aug="augeas"
}

resource = {}
dependencies = {}

function register_resource(restype, name, gota)
  if (not name or not restype) then
    return false, "missing field"
  end
  gota.name = name
  gota.restype = restype
  if (not gota.provider) then
    gota.provider = defaultprovider[restype]
  end

  local uniqueid = string.format("%s:%s", restype, name)

  if (resource[uniqueid]) then
    return false, "duplicate resource"
  end

  resource[uniqueid] = tcopy(gota)

  if (gota.requires) then
    dependencies[uniqueid] = gota.requires
  else
    dependencies[uniqueid] = {}
  end

  return true
end

function audit_resource(res)
  if (resource[res].provider == "apt") then
    return debian.iscompliant(resource[res].name, resource[res])
  elseif (resource[res].provider == "file") then
    return file.iscompliant(resource[res].name, resource[res])
  elseif (resource[res].provider == "augeas") then
    return aug.iscompliant(resource[res].name, resource[res])
  else
    return "UnclueII:"..resource[res]
  end
end

function rectify_resource(res)
  if (resource[res].provider == "apt") then
    return debian.rectify(resource[res].name, resource[res])
  elseif (resource[res].provider == "file") then
    return file.rectify(resource[res].name, resource[res])
  elseif (resource[res].provider == "augeas") then
    return aug.rectify(resource[res].name, resource[res])
  else
    return "Unclue:"..resource
  end
end
