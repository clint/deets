-- deets: inventory.lua
-- Copyright (C) 2010-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--


local z = dpkg.pkglist()
local y
local cs

if(#z > 0) then
  print("debian.package({")

  for _,y in pairs(z) do
    if(dpkg.status(y) == "installed") then
      print('  "'..y..'",')
    end
  end
    print("}, {")
    print('    ensure = "installed",')
    print('    provider = "apt"')
    print('})')
end

for _,y in pairs(z) do
  cs = dpkg.conffilelist(y)
  if(cs) then
    for _,yy in pairs(cs) do
	local type, sm, su, sg
	type, sm, su, sg = deets.tempstat(yy)
        print('file.file({"'..yy..'"}, {')
        print('ensure = "present",')
        print('owner = "'..deets.username(su)..'",')
        print('group = "'..deets.groupname(sg)..'",')
        print('mode = "'..sm..'",')
--      print('source = hmm,')
	print('requires = {"pkg:'..y..'"}')
	print('})')
    end
  end
end

