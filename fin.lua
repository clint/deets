-- deets: fin.lua
-- Copyright (C) 2009-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

require("depresolver")

q, order = resolve_deps(dependencies)

if (deptrace_mode) then
  print("Dep order:")

  for _,y in pairs(order) do
  print(y)
  end
end

if (audit_mode) then
  local rescnt = 0
  local conformant = 0

  for _,y in pairs(order) do
    rescnt = rescnt + 1
    if (audit_resource(y)) then conformant = conformant + 1 end
  end

  print(string.format("Audit: %d/%d (%4.2f%%) conformant", conformant, rescnt, (100*conformant/rescnt)))
end

if (report_mode or rectify_mode) then
  for _,y in pairs(order) do
    if (not audit_resource(y)) then
      if (report_mode) then
        print (rectify_resource(y))
      end
      if (rectify_mode) then
	local cmd = rectify_resource(y)
	local ret
	ret = os.execute(cmd)
        if (ret ~= 0) then
          rectify_mode = false
          report_mode = true
          print("ERROR: `"..cmd.."' exited with "..(ret/256)..".")
          print("Not executing the following commands:")
        end
      end
    end
  end
end

if (delete_tmpdir_mode) then
  os.execute("rm -rf "..tempdir)
end
