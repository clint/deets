/* deets: luau.c
   Copyright (C) 2009-2012  Clint Adams

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#include <sys/stat.h>
#include <unistd.h>

#include <sys/utsname.h>

#include <getopt.h>

#include <sys/socket.h>
#include <netdb.h>

#define LIBDPKG_VOLATILE_API 1
#include <dpkg/dpkg.h>
#include <dpkg/dpkg-db.h>
#include <dpkg/pkg-spec.h>

#define LUA_LIB 1
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include "config.h"

int audit_mode = 1;
int rectify_mode = 0;
int report_mode = 0;
int deptrace_mode = 0;
int delete_tmpdir_mode = 1;
int print_usage = 0;
int print_version = 0;

static int
dpkg_status (lua_State * L)
{
  struct dpkg_error e;
  struct pkginfo *p;
  int j = 0;
  int n = lua_gettop (L);
  int i;
  for (i = 1; i <= n; i++)
    {
      if (!lua_isstring (L, i))
	{
	  lua_pushstring (L, "incorrect argument");
	  lua_error (L);
	}

      p = pkg_spec_parse_pkg (lua_tostring (L, i), &e);
      if (!p)
	{
	  lua_pushstring (L, e.str);
	  lua_error (L);
	}

      lua_pushstring (L, pkg_status_name (p));
      j++;
    }
  return j;
}

static int
dpkg_pkglist (lua_State * L)
{
  struct pkg_hash_iter *iter;
  struct pkginfo *pkg;
  int i = 0;

  lua_newtable (L);

  for (iter = pkg_hash_iter_new (); (pkg = pkg_hash_iter_next_pkg (iter));)
    {
      lua_pushnumber (L, ++i);
      lua_pushstring (L, pkg_name(pkg, pnaw_nonambig));
      lua_rawset (L, -3);
    }
  pkg_hash_iter_free (iter);

  return 1;
}

static int
dpkg_conffilelist (lua_State * L)
{
  struct dpkg_error e;
  struct pkginfo *p;
  struct conffile *c;
  int n = lua_gettop (L);
  int i = 0;

  if (n != 1 || (!lua_isstring (L, 1)))
    {
      lua_pushstring (L, "incorrect argument");
      lua_error (L);
    }

  p = pkg_spec_parse_pkg (lua_tostring (L, 1), &e);
  if (!p)
    {
      lua_pushstring (L, e.str);
      lua_error (L);
    }

  if (!(c = p->installed.conffiles))
    {
      lua_pushnil (L);
      return 1;
    }

  lua_newtable (L);

  do
    {
      lua_pushnumber (L, ++i);
      lua_pushstring (L, c->name);
      lua_rawset (L, -3);
    }
  while (c = c->next);

  return 1;
}

static const luaL_reg dpkglib[] = {
  {"status", dpkg_status},
  {"pkglist", dpkg_pkglist},
  {"conffilelist", dpkg_conffilelist},
  {NULL, NULL}
};

static int
deets_uid_to_name (lua_State * L)
{
  struct passwd *p;

  int n = lua_gettop (L);
  int i;
  int j = 0;
  for (i = 1; i <= n; i++)
    {
      if (!lua_isnumber (L, i))
	{
	  lua_pushstring (L, "incorrect argument");
	  lua_error (L);
	}

      p = getpwuid (lua_tonumber (L, i));
      lua_pushstring (L, p->pw_name);
      j++;
    }

  return j;
}

static int
deets_gid_to_name (lua_State * L)
{
  struct group *g;

  int n = lua_gettop (L);
  int i;
  int j = 0;
  for (i = 1; i <= n; i++)
    {
      if (!lua_isnumber (L, i))
	{
	  lua_pushstring (L, "incorrect argument");
	  lua_error (L);
	}

      g = getgrgid (lua_tonumber (L, i));
      lua_pushstring (L, g->gr_name);
      j++;
    }

  return j;
}

static int
deets_tempstat (lua_State * L)
{
  char m[25];
  struct stat s;

  int n = lua_gettop (L);
  int i;
  int j = 0;
  for (i = 1; i <= n; i++)
    {
      if (!lua_isstring (L, i))
	{
	  lua_pushstring (L, "incorrect argument");
	  j++;
	  lua_error (L);
	}

      if (!lstat (lua_tostring (L, i), &s))
	{
	  if (S_ISREG (s.st_mode))
	    lua_pushstring (L, "file");
	  else if (S_ISDIR (s.st_mode))
	    lua_pushstring (L, "directory");
	  else if (S_ISLNK (s.st_mode))
	    lua_pushstring (L, "link");
	  else
	    lua_pushstring (L, "other");

	  snprintf (m, 24, "%0o", s.st_mode & 007777);
	  lua_pushstring (L, m);
	  lua_pushnumber (L, s.st_uid);
	  lua_pushnumber (L, s.st_gid);
	  j += 4;
	}
      else
	{
	  lua_pushnil (L);
	  j++;
	}
    }

  return j;
}

static const luaL_reg deetslib[] = {
  {"username", deets_uid_to_name},
  {"groupname", deets_gid_to_name},
  {"tempstat", deets_tempstat},
  {NULL, NULL}
};

void
populate_systeminfo (lua_State * L)
{

  int i;
  char buf[64];
  struct utsname ubuf;
  struct addrinfo hints;
  struct addrinfo *carne_de = NULL;

  lua_getfield (L, LUA_GLOBALSINDEX, "deets");
  lua_newtable (L);

  i = gethostname (buf, 63);
  lua_pushstring (L, "hostname");
  if (i)
    lua_pushnil (L);
  else
    {
      buf[63] = '\0';
      lua_pushstring (L, buf);
      memset (&hints, 0, sizeof (struct addrinfo));
      hints.ai_socktype = SOCK_DGRAM;
      hints.ai_flags = AI_CANONNAME;

      if (getaddrinfo (buf, NULL, &hints, &carne_de))
	carne_de = NULL;
    }
  lua_rawset (L, -3);

  lua_pushstring (L, "fqdn");
  if (carne_de && carne_de->ai_canonname)
    lua_pushstring (L, carne_de->ai_canonname);
  else
    lua_pushnil (L);
  lua_rawset (L, -3);

  i = getdomainname (buf, 63);
  lua_pushstring (L, "nis_domainname");
  if (i)
    lua_pushnil (L);
  else
    {
      buf[63] = '\0';
      lua_pushstring (L, buf);
    }
  lua_rawset (L, -3);

  if (uname (&ubuf))
    {
      lua_pushstring (L, "uts_sysname");
      lua_pushnil (L);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_nodename");
      lua_pushnil (L);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_release");
      lua_pushnil (L);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_version");
      lua_pushnil (L);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_machine");
      lua_pushnil (L);
      lua_rawset (L, -3);
    }
  else
    {
      lua_pushstring (L, "uts_sysname");
      lua_pushstring (L, ubuf.sysname);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_nodename");
      lua_pushstring (L, ubuf.nodename);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_release");
      lua_pushstring (L, ubuf.release);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_version");
      lua_pushstring (L, ubuf.version);
      lua_rawset (L, -3);
      lua_pushstring (L, "uts_machine");
      lua_pushstring (L, ubuf.machine);
      lua_rawset (L, -3);
    }

  lua_setfield (L, -2, "systeminfo");

}

int
main (int argc, char **argv)
{
  int c;
  int digit_optind = 0;

  int status;

  char *tempdir;
  char template_dir[] = "/tmp/deets.XXXXXX";	/* use TEMPDIR instead? */

  fprintf (stderr, "deets luau version " PACKAGE_VERSION
	   "\nCopyright (C) 2009-2012  Clint Adams\n");
  fprintf (stderr, "deets comes with ABSOLUTELY NO WARRANTY.\n");
  fprintf (stderr,
	   "This is free software, and you are welcome to redistribute it\n");
  fprintf (stderr, "under certain conditions.\n");

  push_error_context ();
  dpkg_set_progname("luau");

  modstatdb_open (msdbrw_readonly); /* Use default dpkg db directory. */

  while (1)
    {
      int option_index = 0;
      static struct option long_options[] = {
	{"audit", 0, &audit_mode, 1},
	{"rectify", 0, &rectify_mode, 1},
	{"report", 0, &report_mode, 1},
	{"dep-trace", 0, &deptrace_mode, 1},
	{"delete-tmpdir", 0, &delete_tmpdir_mode, 1},
	{"no-audit", 0, &audit_mode, 0},
	{"no-rectify", 0, &rectify_mode, 0},
	{"no-report", 0, &report_mode, 0},
	{"no-dep-trace", 0, &deptrace_mode, 0},
	{"no-delete-tmpdir", 0, &delete_tmpdir_mode, 0},
	{"help", 0, &print_usage, 1},
	{"version", 0, &print_version, 1},
	{0, 0, 0, 0}
      };

      c = getopt_long (argc, argv, "", long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 0:
	case '?':
	  break;

	default:
	  fprintf (stderr, "Warning: ignoring unknown option %c\n", c);
	}
    }

  if (print_usage)
    {
      printf ("\n\nUsage: %s [OPTION]... /path/to/luarfile.lua\n\n", argv[0]);
      printf ("	--audit			report conformance rate (default)\n");
      printf
	("	--rectify		bring system into conformance with model\n");
      printf ("	--report		print what --rectify needs to do\n");
      printf ("	--dep-trace		show dependency ordering\n");
      printf ("	--delete-tmpdir		delete deets tmpdir (default)\n");
      printf ("\nAny option may be prefixed with 'no' to disable it\n");
      printf ("(example: --no-audit)\n");
      exit (0);
    }

  if (print_version)
    {
      printf
	("\nLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n");
      printf ("\nWritten by Clint Adams.\n");
      exit (0);
    }

  setenv ("LUA_PATH",
	  DEETS_LUADIR
	  "/?.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua;/usr/local/lib/lua/5.1/?.lua;/usr/local/lib/lua/5.1/?/init.lua;/usr/share/lua/5.1/?.lua;/usr/share/lua/5.1/?/init.lua",
	  1);

  lua_State *l;
  l = luaL_newstate ();
  luaL_openlibs (l);

  luaL_register (l, "dpkg", dpkglib);
  luaL_register (l, "deets", deetslib);

  lua_pushboolean (l, audit_mode);
  lua_setglobal (l, "audit_mode");
  lua_pushboolean (l, rectify_mode);
  lua_setglobal (l, "rectify_mode");
  lua_pushboolean (l, report_mode);
  lua_setglobal (l, "report_mode");
  lua_pushboolean (l, deptrace_mode);
  lua_setglobal (l, "deptrace_mode");
  lua_pushboolean (l, delete_tmpdir_mode);
  lua_setglobal (l, "delete_tmpdir_mode");

  populate_systeminfo (l);

  tempdir = mkdtemp (template_dir);
  lua_pushstring (l, tempdir);
  lua_setglobal (l, "tempdir");


/*	status = (luaL_loadfile(l, "init.lua") || lua_pcall(l, 0, 0, 0)); */
  status = 999;
  status = (luaL_loadfile (l, DEETS_LUADIR "/init.lua"));
  lua_call (l, 0, 0);
  if (optind < argc)
    {
      while (optind < argc)
	{
	  status = (luaL_loadfile (l, argv[optind++]));
	  lua_call (l, 0, 0);
	}
    }
  else
    {
      status = (luaL_loadfile (l, DEETS_LUADIR "/default.lua"));
      lua_call (l, 0, 0);
    }
  status = (luaL_loadfile (l, DEETS_LUADIR "/fin.lua"));
  lua_call (l, 0, 0);

  lua_close (l);

  pop_error_context (ehflag_normaltidy);

  return 0;
}
