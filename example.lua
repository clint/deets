-- deets: example.lua
-- Copyright (C) 2009-2012  Clint Adams

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

file.file({"/tmp/skrink/coccyx"}, {
    ensure = "present",
    owner = "clint",
    group = "jones",
    mode = "4755",
    source = "/etc/passwd",
    requires = {"pkg:zsh", "file:/tmp/skrink"}
})

file.file({"/tmp/tramps"}, {
    ensure = "present",
    owner = "root",
    group = "root",
    mode = "755",
    source = file.cosmotemplate("/tmp/tramps.cosmo", {}),
    requires = {"pkg:zsh"}
})

debian.package({"zsh", "posh", "vim"}, {
    ensure = "installed",
    provider = "apt"
})

debian.package({"tomcat5.5", "nano", "libmalaga7", "libxtrap6"}, {
    ensure = "purged",
    provider = "apt"
})

file.file({"/etc/hosts","/etc/resolv.conf"}, {
    ensure = "present",
    owner = "root",
    group = "root",
    mode = "644"
})

file.directory({"/tmp/skrink"}, {
    ensure = "present",
    owner = "clint",
    group = "jones",
    mode = "755",
    requires = {"pkg:zsh"}
})

file.file({"/tmp/regis"}, {
    ensure = "absent"
})

aug.root({"/files/etc/ssh/sshd_config/X11Forwarding"}, {
    ensure = "unset"
})

aug.root({"/files/etc/ssh/ssh_config/Host/HashKnownHosts"}, {
    ensure = "set",
    value = "no"
})
